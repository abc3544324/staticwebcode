package com.amdocs;
import static org.junit.Assert.*;
import org.junit.Test;

public class IncrementTest {
    @Test
    public void testdecreasecounter() throws Exception {

        int k= new Increment().decreasecounter(0);
        assertEquals("decreased", 1, k);
        
    }
   
   @Test
    public void testcounter() throws Exception {

        int k= new Increment().decreasecounter(1);
        assertEquals("added", 1, k);
        
    }
	 @Test
    public void testincreasecounter() throws Exception {

        int k= new Increment().decreasecounter(3);
        assertEquals("added", 1, k);
        
    }
}
